import React from "react";
import { render } from "react-dom";
import App from "./App";
import store from "./store";
import { Provider } from "react-redux";
import "./assets/reset.scss";
import "./index.scss";

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector(".chat-client")
);
