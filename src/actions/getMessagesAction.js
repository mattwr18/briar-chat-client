import { GET_MESSAGES } from "../types/index";
import { addMessage } from "../actions/postMessageAction";
import axios from "axios";

export let getMessages = messages => {
  console.log(messages);
  return {
    type: GET_MESSAGES,
    payload: messages
  };
};

export let fetchMessages = () => dispatch => {
  axios({
    method: "GET",
    url: "http://127.0.0.1:7000/v1/messages/2",
    headers: {
      Authorization: `Bearer ${process.env.BRIAR_AUTH_TOKEN}`
    }
  }).then(res => dispatch(getMessages(res.data)));
};

export let getMessageFromWebsocket = message => dispatch =>
  dispatch(addMessage(message));
