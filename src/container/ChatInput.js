import React, { Component } from "react";
import axios from "axios";
import { postMessage } from "../actions/postMessageAction";
import { connect } from 'react-redux';
import "../assets/ChatInput.scss";

class ChatInput extends Component {
  state = { text: "" };
  handleChange = event => {
    this.setState({
      text: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.props.postMessage(this.state.text)
    this.setState({ text: "" });
  };

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          value={this.state.text}
          onChange={this.handleChange}
          className="chat-input"
          placeholder="Message Josepovic"
        />
      </form>
    );
  }
}


export default connect(
  null,
  { postMessage }  
)(ChatInput);
