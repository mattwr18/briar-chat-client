import React, { Component } from "react";
import { connect } from "react-redux";
import Message from "../components/Message";
import { fetchMessages, getMessageFromWebsocket } from "../actions/getMessagesAction";
export class MessageList extends Component {
  static defaultProps = { messages: [] };
  componentDidMount() {
    this.props.fetchMessages();
    this.mounted = true;
    var token = process.env.BRIAR_AUTH_TOKEN;

    this.socket = new WebSocket("ws://localhost:7000/v1/ws");

    this.socket.onopen = () => {
      this.socket.send(token);
    };

    this.socket.onmessage = event => {
      if (this.mounted) {
        this.props.getMessageFromWebsocket(JSON.parse(event.data).data)
      }
    };
  }

  handleUpdateMessage = message => {
    this.setState(prevState => ({
      messages: [...prevState.messages, message]
    }));
  };

  componentWillUnmount() {
    this.mounted = false;
    this.socket.close();
  }

  render() {
    const { messages } = this.props;
    console.log(this.props);
    let elements;
    if (messages.length > 0) {
      elements = messages.map(message => {
        return <Message key={message.id} message={message} />;
      });
    }
    return (
      <React.Fragment>
        {elements}
      </React.Fragment>
    );
  }
}

const mapStateToProps = store => ({ messages: store.messages });
export default connect(
  mapStateToProps,
  { fetchMessages, getMessageFromWebsocket }
)(MessageList);
