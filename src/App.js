import React, { Component } from "react";
import MessageList from "./container/MessageList";
import ChatInput from "./container/ChatInput";
import ContactList from "./container/ContactList";
class App extends Component {
  render() {
    return (
      <React.Fragment>
        <ContactList />
        <MessageList />
        <ChatInput />
      </React.Fragment>
    );
  }
}

export default App;
